from django.urls import path
from food.views import recipe_list, show_recipe

urlpatterns = [
    path("", recipe_list, name="recipe_list"),
    path("<int:id>/", show_recipe, name="show_recipe"),
]
